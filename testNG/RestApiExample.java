package com.infosys.demos.TestNGMaven.restapi;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

@Test(singleThreaded = true)
public class RestApiExample {
	
	private RestTemplate restTemplate = new RestTemplate();
	private ObjectMapper objMapper = new ObjectMapper();
	
	@Test(enabled = false)
	  public void getFile() {
		  try {
			  System.out.println("Get Request via File");
			  
//			  //Get JSON to be read
			  File getResponse = new File("src/test/java/com/infosys/demos/TestNGMaven/restapi/getResult.json");

			  //Read through JSON
			  List<LinkedHashMap<String, String>> response = JsonPath.read(getResponse, "$.data[0:24]");
			  for (LinkedHashMap <String, String> employee : response) {
				  System.out.println(employee);
			  }
		  } catch (IOException exc) {
			  exc.printStackTrace();
			  fail();
		  }
		  System.out.println();
	  }
	
	@Test(enabled = true)
	public void getRestTemplate() {
		System.out.println("Get Request via RestTemplate");
		
		//Get JSON to be read
		ResponseEntity<String> restResponse = restTemplate.getForEntity("http://dummy.restapiexample.com/api/v1/employees", String.class);
		System.out.println(restResponse.getBody());
		
		//Read through JSON
		String status = JsonPath.read(restResponse.getBody(), "$.status");
		assertEquals(status, "success");
		
		List<LinkedHashMap<String, Object>> response = JsonPath.read(restResponse.getBody(), "$.data[0:2]");
		assertNotNull(response);
		assertFalse(response.isEmpty());
		Set<Integer> ids = new HashSet<Integer>();
		for (LinkedHashMap <String, Object> employee : response) {
			System.out.println(employee);
			assertNotNull(employee.get("employee_name"));
			
			Integer employeeAge = (Integer)employee.get("employee_age");
			assertNotNull(employeeAge);
			assertTrue(employeeAge > 0);
			
			Integer employeeSalary = (Integer)employee.get("employee_salary");
			assertNotNull(employeeSalary);
			assertTrue(employeeSalary > 0);
			
			Integer employeeId = (Integer)employee.get("id");
			assertNotNull(employeeId);
			assertFalse(ids.contains(employeeId));
			if (!ids.contains(employeeId)) {
				ids.add(employeeId);
			}
		}
		System.out.println();
	}
	
	@Test(enabled = false)
	  public void postFile() {
		  try {
			  System.out.println("Post Request Test");
			  //Get JSON to be read through
			  File getResponse = new File("src/test/java/com/infosys/demos/TestNGMaven/restapi/postResult.json");
			  
			  //Read through JSON
			  LinkedHashMap<String, String> response = JsonPath.read(getResponse, "$");
			  System.out.println(response);
		  } catch (IOException exc) {
			  exc.printStackTrace();
			  fail();
		  }
		  System.out.println();
	  }
	
	@Test(enabled = true)
	public void postRestTemplate() {
		System.out.println("Post Request via RestTemplate");
		
		EmployeeDTO newEmp = new EmployeeDTO("test", 123, 23, "");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<EmployeeDTO> entity = new HttpEntity<>(newEmp, headers);
		System.out.println(entity);

		//Get JSON to be read
		ResponseEntity<String> restResponse = restTemplate.exchange("http://dummy.restapiexample.com/api/v1/create", HttpMethod.POST, entity, String.class);
		System.out.println(restResponse.getBody());
		
		//Read through JSON
		LinkedHashMap<String, String> response = JsonPath.read(restResponse.getBody(), "$");
		assertNotNull(response);
		System.out.println(response);
		assertFalse(response.isEmpty());
		assertEquals(response.get("status"), "success");
		assertTrue(response.get("message").contains("Success"));
		
		LinkedHashMap<String, Object> data = JsonPath.read(restResponse.getBody(), "$.data");
		assertEquals(data.get("name"), newEmp.getName());
		assertEquals(data.get("age"), newEmp.getAge());
		assertEquals(data.get("salary"), newEmp.getSalary());
		
		System.out.println();
	}
	
	@Test(enabled = false)
	  public void deleteFile() {
		  try {
			  System.out.println("Delete Request Test");
			  //Get JSON to be read through
			  File getResponse = new File("src/test/java/com/infosys/demos/TestNGMaven/restapi/deleteResult.json");
			  
			  //Read through JSON
			  LinkedHashMap<String, String> response = JsonPath.read(getResponse, "$");
			  System.out.println(response);
		  } catch (IOException exc) {
			  exc.printStackTrace();
			  fail();
		  }
		  System.out.println();
	  }
	
	@Test(enabled = true)
	public void deleteRestTemplate() {
		System.out.println("Delete Request via RestTemplate");
		
		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("id", new Integer(1));
		
		ResponseEntity<String> restResponse = restTemplate
												.exchange("http://dummy.restapiexample.com/api/v1/delete/{id}", 
														HttpMethod.DELETE, 
														null, 
														String.class, 
														uriVariables);
		assertNotNull(restResponse);
		System.out.println(restResponse.getBody());
		LinkedHashMap<String, Object> response = JsonPath.read(restResponse.getBody(), "$");
		assertNotNull(response.get("status"));
		assertEquals(response.get("status"), "success");
		
		assertNotNull(response.get("data"));
		assertEquals(response.get("data"), "" + uriVariables.get("id"));
		System.out.println();
	}
	
}
