package com.infosys.demos.TestNGMaven.restapi;

public class EmployeeDTO {

	public String name;
	public int salary;
	public int age;
	public String profile_image;
	
	public EmployeeDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public EmployeeDTO(String employee_name, int employee_salary, int employee_age, String profile_image) {
		super();
		this.name = employee_name;
		this.salary = employee_salary;
		this.age = employee_age;
		this.profile_image = profile_image;
	}

	public String getName() {
		return name;
	}

	public void setName(String employee_name) {
		this.name = employee_name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int employee_salary) {
		this.salary = employee_salary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int employee_age) {
		this.age = employee_age;
	}

	public String getProfile_image() {
		return profile_image;
	}

	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [" + (name != null ? "employee_name=" + name + ", " : "")
				+ "employee_salary=" + salary + ", employee_age=" + age + ", "
				+ (profile_image != null ? "profile_image=" + profile_image : "") + "]";
	}
}
