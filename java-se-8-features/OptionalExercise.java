import java.util.Arrays;
import java.util.Optional;

class EmployeeOptional{
	private Optional<String> projectCode = Optional.empty();

	public Optional<String> getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(Optional<String> projectCode) {
		this.projectCode = projectCode;
	}

	public EmployeeOptional(Optional<String> projectCode) {
		super();
		this.projectCode = projectCode;
	}
	
}
public class OptionalExercise {

	public static void main(String[] args) {
		EmployeeOptional[] emps = {new EmployeeOptional(Optional.of("Apple")), 
				new EmployeeOptional(Optional.of("Amazon")), 
				new EmployeeOptional(Optional.empty())};
		Arrays.stream(emps)
			.filter(emp -> emp.getProjectCode().isPresent())
			.forEach(emp -> emp.setProjectCode(Optional.of(emp.getProjectCode().get() + " BestSol")));
		for (EmployeeOptional emp : emps) {
			System.out.println(emp.getProjectCode().orElse("On bench"));
		}
	}

}
