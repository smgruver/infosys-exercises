import java.util.function.BiFunction;

public class CalculatorFunctions {

	public static void main (String pars[]) {
		int x = 2;
		int y = 3;
		
        // call evaluate for adding two int values
		System.out.println(evaluate(x, y, (int1, int2) -> int1 + int2));
        // call evaluate for subtracting two int values
		System.out.println(evaluate(x, y, (int1, int2) -> int1 - int2));
        // call evaluate for multiplying two int values
		System.out.println(evaluate(x, y, (int1, int2) -> int1 * int2));
    }
 
    public static Integer evaluate(Integer t, Integer u, BiFunction<Integer, Integer, Integer> fn) {
        return fn.apply(t, u);
    }

}
