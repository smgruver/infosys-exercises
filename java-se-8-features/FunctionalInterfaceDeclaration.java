@FunctionalInterface
public interface FunctionalInterfaceDeclaration {
	String format(String string1, String string2);
	
	public static void main(String[] args) {
		String str1 = "Hello";
		String str2 = "world";
		FunctionalInterfaceDeclaration strFormatter = ((x, y) -> x + " " + y);
		System.out.println(strFormatter.format(str1, str2));
		
		strFormatter = ((x, y) -> x + " - " + y);
		System.out.println(strFormatter.format(str1, str2));
		
		strFormatter = ((String x, String y) -> x.toUpperCase() + " " + y.toUpperCase());
		System.out.println(strFormatter.format(str1, str2));
	}
}
