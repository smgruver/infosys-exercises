import java.util.Arrays;

class Employee {
    public int id;
    public String name;
    private int sal;
    private double yearsInOrg;
    private String role;
    private String gender;
 
    public Employee(String name, int id, int sal, double years, String role, String gender) {
        this.id = id;
        this.name = name;
        this.sal = sal;
        this.yearsInOrg = years;
        this.role = role;
        this.gender = gender;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSal() {
		return sal;
	}

	public void setSal(int sal) {
		this.sal = sal;
	}

	public double getYearsInOrg() {
		return yearsInOrg;
	}

	public void setYearsInOrg(double yearsInOrg) {
		this.yearsInOrg = yearsInOrg;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}

public class StreamIntro {

	public static void main(String[] args) {
		Employee[] empArr = {new Employee("a", 1, 1, 1.0, "one", "male"), new Employee("b", 2, 2, 2.0, "two", "female")};
		System.out.println("Printing all employees with role \"one\"");
		Arrays.stream(empArr).filter(emp -> emp.getRole().equals("one")).forEach(emp -> System.out.println(emp.getName()));
		System.out.println("Printing count of female employees");
		long femCount = Arrays.stream(empArr).filter(emp -> emp.getGender().equals("female")).count();
		System.out.println(femCount);
	}

}
