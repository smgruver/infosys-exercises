
public class LambdaExpressions {

	public static void main(String[] args) {
		Thread threadInstance = new Thread(new Runnable() {
            //run --- implementation
            public void run() {
                System.out.println(" Its anonymous class from thread");
            }
        });
		threadInstance.start();
		
		Thread threadInstance2 = new Thread(() -> System.out.println(" Its lambda from thread"));
        threadInstance2.start();
	}
}
