import java.util.stream.Stream;

public class StreamReduce {

	public static void main(String[] args) {
		int sum = Stream.iterate(1, n -> n + 1).limit(50).reduce(0, (total, next) -> total += next);
		System.out.println("Sum 1-50: " + sum);
	}
}
