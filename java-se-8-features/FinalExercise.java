import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
 * Name of the guest
 * Date Of Birth
 * Country – Assumption: Guests visit only from the following countries
 * USA, Spain, Germany, France, China
 * Language – Guests speak only one of the below languages
 * English, Spanish, French, Chinese, German
 * Hobby – Need to be from the following
 * Eat, Drink, Sports, Dance, Travel, Read, Music
 * Create a Guest class to hold the data for the guest having name, dob, country, language and hobby as attributes.
 */
class Guest {
	private String name = null;
	private LocalDate dob = null;
	private Country country = null;
	private Language language = null;
	private Hobby hobby = null;
	
	public Guest() {
	}
	
	public Guest(String name, LocalDate dob, Country country, Language language, Hobby hobby) {
		super();
		this.name = name;
		this.dob = dob;
		this.country = country;
		this.language = language;
		this.hobby = hobby;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Hobby getHobby() {
		return hobby;
	}

	public void setHobby(Hobby hobby) {
		this.hobby = hobby;
	}

	@Override
	public String toString() {
		return "Guest [name=" + name + ", dob=" + dob + ", country=" + country + ", language=" + language + ", hobby="
				+ hobby + "]";
	}
	
}
enum Country {
	USA, SPAIN, GERMANY, FRANCE, CHINA;
}
enum Language {
	ENGLISH, SPANISH, GERMAN, FRENCH, CHINESE;
}
enum Hobby {
	EAT, DRINK, SPORTS, DANCE, TRAVEL, READ, MUSIC;
}

/*
 * Create another main class named EventManager
 * In the main method of this class invoke the below mentioned methods in step 2 and step 3
 * Define a method called populateGuests to populate a List, containing Guest objects having different values
 * Define another method called filterGuest for filtering the guest
 */
public class FinalExercise {
	List<Guest> guests;

	public static void main(String[] args) {
		FinalExercise eventManager = new FinalExercise();
		eventManager.guests = eventManager.populateList(new ArrayList<Guest>());
		//From Spain and likes dance or music OR likes drinking and is older than 18
		System.out.println(eventManager.filterGuest(eventManager.guests, 
				(Guest g) -> (Country.SPAIN.equals(g.getCountry()) 
						&& Hobby.DANCE.equals(g.getHobby()) || Hobby.MUSIC.equals(g.getHobby())
						|| Hobby.DRINK.equals(g.getHobby()) && Period.between(g.getDob(), LocalDate.now()).getYears() >= 18)));
	}
	
	private List<Guest> populateList(List<Guest> toPopulate){
		toPopulate.clear();
		toPopulate.add(new Guest("Adam", LocalDate.of(1997, 10, 28), Country.USA, Language.ENGLISH, Hobby.EAT));
		toPopulate.add(new Guest("Bob", LocalDate.of(1991, 1, 1), Country.SPAIN, Language.SPANISH, Hobby.DRINK));
		toPopulate.add(new Guest("Claire", LocalDate.of(1992, 2, 2), Country.FRANCE, Language.FRENCH, Hobby.SPORTS));
		toPopulate.add(new Guest("Dan", LocalDate.of(1993, 3, 3), Country.GERMANY, Language.GERMAN, Hobby.DANCE));
		toPopulate.add(new Guest("Eve", LocalDate.of(1994, 4, 4), Country.CHINA, Language.CHINESE, Hobby.TRAVEL));
		toPopulate.add(new Guest("Frank", LocalDate.of(1995, 5, 5), Country.USA, Language.SPANISH, Hobby.READ));
		toPopulate.add(new Guest("Gina", LocalDate.of(1996, 6, 6), Country.SPAIN, Language.FRENCH, Hobby.MUSIC));
		return toPopulate;
	}
	
	private List<Guest> filterGuest(List<Guest> toFilter, Predicate<Guest> condition){
		return toFilter.stream().filter(condition).collect(Collectors.toList());
	}

}
