package com.infosys.demos.TestNGMaven.jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator.Attribute;

/**
 * Example program to list links from a URL.
 */
public class JsoupReadingFromUrl {
    public static void main(String[] args) throws IOException {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Enter the URL to be fetched: ");
        String url = in.nextLine().trim();
        print("Fetching %s...", url);
        
        Document doc = Jsoup.connect(url).get();

        String[] options = {"Print Media", "Print Imports", "Print Links", "Print Elements", "Print Text", "Print Meta"};
        short userInput = -1;
        do {
        	for (int i = 0; i < options.length; i++) {
        		System.out.println(i + ": " + options[i]);
        	}
        	System.out.println("What would you like to do?\nEnter 0-" + (options.length - 1) + ", or -1 to quit: ");
        	
        	userInput = Short.parseShort(in.next());
        	
        	switch(userInput) {
        	case 0: 
        		printMedia(doc);
        		break;
        	case 1:
        		printImports(doc);
        		break;
        	case 2:
        		printLinks(doc);
        		break;
        	case 3:
        		printElements(doc);
        		break;
        	case 4:
        		printText(doc);
        		break;
        	case 5:
        		printMeta(doc);
        		break;
    		default:
    			return;
        	}
        	System.out.println();
        } while (userInput >= 0 && userInput < options.length);

    }
    
    /*
     * Prints all media by finding all elements with a "src" attribute
     */
    private static void printMedia(Document doc) {
    	Elements media = doc.select("[src]");
    	print("\nMedia: (%d)", media.size());
    	for (Element src : media) {
    		if (src.nodeName().equalsIgnoreCase("img"))
    			print(" * %s: <%s> %sx%s (%s)",
    					src.tagName(), src.attr("abs:src"), src.attr("width"), src.attr("height"),
    					trim(src.attr("alt"), 20));
    		else
    			print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
    	}	
    }
    
    /*
     * Prints all imports by finding all "link" elements with "href" attributes
     */
    private static void printImports(Document doc) {
    	Elements imports = doc.select("link[href]");
    	print("\nImports: (%d)", imports.size());
    	for (Element link : imports) {
    		print(" * %s <%s> (%s)", link.tagName(),link.attr("abs:href"), link.attr("rel"));
    	}    	
    }
    
    /*
     * Prints all links by finding "a" elements with an "href" attribute
     */
    private static void printLinks(Document doc) {
    	Elements links = doc.select("a[href]");
    	print("\nLinks: (%d)", links.size());
    	for (Element link : links) {
    		print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
    	}    	
    }
    
    /*
     * Prints all elements' tags, id (if any), classes (if any), and text (if any)
     */
    private static void printElements(Document doc) {
    	Elements elements = doc.getAllElements();
    	for (Element el : elements) {
    		System.out.print(String.join("", Collections.nCopies(el.parents().size(), ". ")));
    		print("%s [%s] (%s): %s", el.tagName(), el.id(), el.className(), el.ownText());
    	}
    }
    
    /*
     * Prints all elements that directly contain text that starts with a real character.
     */
    private static void printText(Document doc) {
    	Elements textElements = doc.getElementsMatchingOwnText("^[a-zA-Z0-9]+");
    	for (Element elWithText : textElements) {
    		System.out.println(elWithText.text());
    	}
    }
    
    /*
     * Prints the meta tags and their attributes.
     */
    private static void printMeta(Document doc) {
    	Elements metaElements = doc.getElementsByTag("meta");
    	for (Element metaEl : metaElements) {
    		print("%s [%s] (%s)", metaEl.tagName(), metaEl.id(), metaEl.className());
    		for (org.jsoup.nodes.Attribute attr : metaEl.attributes()) {
    			print(" . %s = %s", attr.getKey(), attr.getValue());
    		}
    	}
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }
}
