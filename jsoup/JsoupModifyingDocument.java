package com.infosys.demos.TestNGMaven.jsoup;

import java.nio.charset.StandardCharsets;

import org.jsoup.nodes.Document;

public class JsoupModifyingDocument {

	public static void main(String[] args) {
		Document myDoc = new Document(".\\mydoc.html");		//Empty document
//		System.out.println(myDoc);
		
		myDoc.appendElement("html").appendElement("head");
		myDoc.selectFirst("html").appendElement("body");
//		System.out.println(myDoc.html());
		/*
		 * <html>
		 *  <head></head>
		 *  <body></body>
		 * </html>
		 */
		
		myDoc.charset(StandardCharsets.UTF_8);
		myDoc.title("Jsoup-Created Document");
		myDoc.body().appendElement("p").text("Some Text.").addClass("plain_text");
//		System.out.println(myDoc.html());
		/*
		 * <html>
		 *  <head>
		 *   <meta charset="UTF-8">
		 *   <title>Jsoup-Created Document</title>
		 *  </head>
		 *  <body>
		 *   <p class="plain class">Some Text</p>
		 *  </body>
		 * </html>
		 */
		
		myDoc.selectFirst("p.plain_text").wrap("<div id=\"first_div\"></div>").append("<u> Some more text.</u>");
		//Appends img element to the inside of the p element, not the div element
		myDoc.selectFirst("div#first_div").append("<img src=myImg.png></img>");
		System.out.println(myDoc.html());
		/*
		 * <html>
		 *  <head>
		 *   <meta charset="UTF-8">
		 *   <title>Jsoup-Created Document</title>
		 *  </head>
		 *  <body>
		 *   <div id="first_div">
		 *    <p class="plain_text">Some Text.<u>Some more text.</u></p>
		 *    <img src="myImg.png">
		 *   </div>
		 *  </body>
		 * </html>
		 */
	}
}
