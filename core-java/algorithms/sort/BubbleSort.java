package algorithms.sort;

import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {
		Integer[] arr = {3, 8, 1, 0, 6, 10, 38, 2};
		System.out.println("Beginning Bubble Sort on " + Arrays.asList(arr));
		System.out.println(Arrays.asList(bubbleSort(arr)));
	}

	/**
	 * O(n^2) sorting algorithm for an array
	 * @param arr	The array to be sorted
	 * @return		The same array, now sorted.
	 */
	public static Integer[] bubbleSort(Integer[] arr) {
		boolean unsorted;
		int temp, current, next;
		
		do {
			unsorted = false;
			for (int i = 0; i < arr.length - 1; i++) {
				current = arr[i];
				next = arr[i+1];
				
				if (current > next) {
					temp = current;
					arr[i] = next;
					arr[i+1] = temp;
					unsorted = true;
				}
			}	
		} while (unsorted);

		return arr;
	}
	
}
