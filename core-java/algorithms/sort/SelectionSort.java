package algorithms.sort;

import java.util.Arrays;
import java.util.Collections;

public class SelectionSort {

	public static void main(String[] args) {
		Integer[] arr = {3, 8, 1, 0, 6, 10, 38, 2};
		System.out.println("Beginning Selection Sort on " + Arrays.asList(arr));
		System.out.println(Arrays.asList(selectionSort(arr)));
	}

	public static Integer[] selectionSort(Integer[] arr) {
		int temp, lowestIndex, next;
		
		for (int i = 0; i < arr.length; i++) {
			//Finding the smallest element in the unsorted range
			lowestIndex = i;
			for (int j = i; j < arr.length; j++) {
				if (arr[j] < arr[lowestIndex]) {
					lowestIndex = j;
				}
			}
//			System.out.println("Lowest index found in " + Arrays.asList(Arrays.copyOfRange(arr, i, arr.length)) + " is " + (lowestIndex - i));
			
			next = arr[i];
			
			if (arr[lowestIndex] < next) {
				temp = arr[lowestIndex];
				arr[lowestIndex] = next;
				arr[i] = temp;
			}
		}	

		return arr;
	}
	
}
