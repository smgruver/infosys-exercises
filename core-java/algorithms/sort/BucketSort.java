package algorithms.sort;

import java.util.Arrays;
import java.util.Collections;

public class BucketSort {

	public static void main(String[] args) {
		Integer[] arr1 = {3, 8, 1, 0, 6, 10, 38, 2};
		System.out.println("Beginning Bucket Sort on " + Arrays.asList(arr1));
		System.out.println(Arrays.asList(bucketSortNoDups(arr1, Collections.max(Arrays.asList(arr1)))));
		
		Integer[] arr2 = {3, 8, 1, 3, 0, 6, 1, 10, 38, 2, 2};
		System.out.println("Beginning Bucket Sort on " + Arrays.asList(arr2));
		System.out.println(Arrays.asList(bucketSortWithDups(arr2, Collections.max(Arrays.asList(arr2)))));
	}

	/**
	 * O(n) sorting algorithm for positive-only, no-duplicate arrays. 
	 * VERY costly with memory space, requires knowledge of highest present value.
	 * @param arr	The array to be sorted. Must be positive-only and contain no duplicates
	 * @param max	The highest value present in the array
	 * @return		A new array containing arr's elements in ascending order.
	 */
	public static Integer[] bucketSortNoDups(Integer[] arr, int max) {
		Integer[] bucket = new Integer[max + 1];
		Integer[] result = new Integer[arr.length];
		
		for (int i = 0; i < arr.length; i++) {	
			//uses the array of size equal to the maximum value like a map, 
			//where the index is the key and a non-null (or non-zero) value means the value of the index is present
			bucket[arr[i]] = 1;
		}
		
		int resultIndex = 0;
		for (int i = 0; i <= max; i++) {
			if (bucket[i] != null) {
				result[resultIndex] = i;
				resultIndex++;
			}
		}
		
		return result;
	}
	
	/**
	 * O(n) sorting algorithm for positive-only. 
	 * VERY costly with memory space, requires knowledge of highest present value.
	 * @param arr	The array to be sorted. Must be positive-only and contain no duplicates
	 * @param max	The highest value present in the array
	 * @return		A new array containing arr's elements in ascending order.
	 */
	public static Integer[] bucketSortWithDups(Integer[] arr, int max) {
		Integer[] bucket = new Integer[max + 1];
		Integer[] result = new Integer[arr.length];
		
		for (int i = 0; i < arr.length; i++) {	
			//uses the array of size equal to the maximum value like a map, 
			//where the index is the key and a non-null (or non-zero) value means the value of the index is present bucket[i] times
			if (bucket[arr[i]] == null) {
				bucket[arr[i]] = 0;
			}
			bucket[arr[i]]++;
		}
		
		int resultIndex = 0;
		for (int i = 0; i <= max; i++) {
			if (bucket[i] != null) {
				for (int j = 0; j < bucket[i]; j++) {
					result[resultIndex] = i;
					resultIndex++;
				}
			}
		}
		
		return result;
	}
	
}
