package algorithms.sort;

import java.util.Arrays;

public class MergeSort {

	public static void main(String[] args) {
		Integer[] arr = {3, 8, 1, 0, 6, 10, 38, 2};
		System.out.println("Beginning Merge Sort on " + Arrays.asList(arr));
		System.out.println(Arrays.asList(mergeSort(arr, 0, arr.length - 1)));
	}

	public static Integer[] mergeSort(Integer[] arr, int low, int high) {
//		System.out.println("Performing merge sort on " + Arrays.asList(Arrays.copyOfRange(arr, low, high + 1)));
		
		if (high <= low) {
			return arr;
		} else {
			int mid = (high - low)/2 + low;
			mergeSort(arr, low, mid);
			mergeSort(arr, mid + 1, high);
//			System.out.println("Merging " + Arrays.asList(Arrays.copyOfRange(arr, low, mid + 1)) + " and " + Arrays.asList(Arrays.copyOfRange(arr, mid + 1, high + 1)));
			return merge(arr, low, mid, high);
		}
	}
	
	private static Integer[] merge(Integer[] arr, int low, int mid, int high) {
		int leftLength = (mid - low + 1);
		int rightLength = (high - mid);
		
		//Copying left subarrary into new array
		Integer[] left = new Integer[leftLength];
		for (int i = 0; i < leftLength; i++) {
			left[i] = arr[low + i];
		}
//		System.out.println("Left: " + Arrays.asList(left));
		
		//Copying right subarray into new array
		Integer[] right = new Integer[rightLength];
		for (int i = 0; i < rightLength; i++) {
			right[i] = arr[mid + 1 + i];
		}
//		System.out.println("Right: " + Arrays.asList(right));
		
		int leftIndex = 0;
		int rightIndex = 0;
		for (int i = low; i < high + 1; i++) {
//			System.out.println("i = " + i + ", leftIndex = " + leftIndex + ", rightIndex = " + rightIndex);
			if (leftIndex < leftLength && rightIndex < rightLength) {
				if (left[leftIndex] < right[rightIndex]) {
					arr[i] = left[leftIndex];
					leftIndex++;
				} else {
					arr[i] = right[rightIndex];
					rightIndex++;
				}
			} else if (leftIndex >= leftLength) {
				arr[i] = right[rightIndex];
				rightIndex++;
			} else {	//rightIndex >= rightLength
				arr[i] = left[leftIndex];
				leftIndex++;
			}
		}
		
		return arr;
	}
	
}
