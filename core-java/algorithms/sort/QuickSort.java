package algorithms.sort;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {
		Integer[] arr1 = {3, 8, 1, 0, 6, 10, 38, 2};
		System.out.println("Beginning Quick Sort with High Partition on " + Arrays.asList(arr1));
		System.out.println(Arrays.asList(quickSortHighPartition(arr1, 0, arr1.length - 1)));
		
		Integer[] arr2 = {3, 8, 7, 0, 11, 10, 19, 4, 5};
		System.out.println("Beginning Quick Sort with Middle Partition on " + Arrays.asList(arr2));
		System.out.println(Arrays.asList(quickSortMiddlePartition(arr2, 0, arr2.length - 1)));
	}
	
	public static Integer[] quickSortHighPartition(Integer[] arr, int low, int high) {
		if (high <= low) {
			return arr;
		} else {
//			System.out.println("Performing quickSort on " + Arrays.asList(Arrays.copyOfRange(arr, low, high+1)));
			int pivot = highPartition(arr, low, high);
//			System.out.println("Post-partition: " + Arrays.asList(Arrays.copyOfRange(arr, low, high+1)) + ", pivot moved to " + (pivot - low));
			quickSortHighPartition(arr, low, pivot - 1);
			quickSortHighPartition(arr, pivot + 1, high);
		}
		
		return arr;
	}
	
	private static int highPartition(Integer[] arr, int low, int high) {
		int pivot = high;	//tracks the position where the pivot IS before the section gets partitioned.
		int counter = low;	//tracks the position where the pivot WILL be when this section is done being partitioned.
		
		//make sure that elements smaller than the pivot element will be bunched together on one side
		//via swapping
		for (int i = low; i < high; i++) {
			if (arr[i] < arr[pivot]) {
				int temp = arr[counter];
				arr[counter] = arr[i];
				arr[i] = temp;
				counter++;
			}
		}
		//Move the pivot element to be after the bunched up smaller elements
		int temp = arr[pivot];
		arr[pivot] = arr[counter];
		arr[counter] = temp;
		
		return counter;
	}
	
	public static Integer[] quickSortMiddlePartition(Integer[] arr, int low, int high) {
		if (high <= low) {
			return arr;
		} else {
//			System.out.println("Performing quickSort on " + Arrays.asList(Arrays.copyOfRange(arr, low, high+1)));
			int pivot = middlePartition(arr, low, high);
//			System.out.println("Post-partition: " + Arrays.asList(Arrays.copyOfRange(arr, low, high+1)) + ", splits after " + (pivot - low));
			quickSortMiddlePartition(arr, low, pivot);		//The pivot is included in one of the subarrays with a middle partition
			quickSortMiddlePartition(arr, pivot + 1, high);
		}
		
		return arr;
	}
	
	private static int middlePartition(Integer[] arr, int low, int high) {
		int pivotIndex = (high - low)/2 + low;
		int pivotValue = arr[pivotIndex];
//		System.out.println("Pivot selected: " + pivotValue + " at " + pivotIndex);
		int leftCounter = low, rightCounter = high;
		
		//iterate from both directions, swapping positions of values as necessary to maintain the partition
		while (leftCounter <= rightCounter) {
			while (arr[leftCounter] < pivotValue) {	//iterate leftCounter until a value greater than the pivot
				leftCounter++;
			}
			while (arr[rightCounter] > pivotValue) {	//iterate rightCounter until a value less than the pivot
				rightCounter--;
			}
			if (leftCounter <= rightCounter) {	//if they haven't yet passed each other, swap the values they're looking at
				int temp = arr[leftCounter];
				arr[leftCounter] = arr[rightCounter];
				arr[rightCounter] = temp;
				leftCounter++;
				rightCounter--;
			}
		}
		
		return leftCounter - 1;
	}
}
