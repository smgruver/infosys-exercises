package algorithms.search;

import java.util.Arrays;

/*
 * Searches a collection by jumping forward by fixed intervals until the element is found or passed.
 * If the element is passed, goes back one interval and performs a linear search from there.
 */
public class JumpSearch {

	public static void main(String[] args) {
		Integer[] arr = {1, 2, 4, 8, 9, 12, 29, 102};
		int jump = 4;
		int target = 12;
		
		System.out.println("Beginning Jump Search on " + Arrays.asList(arr) + " for target " + target);
		System.out.println(jumpSearch(arr, jump, target));
	}
	
	/**
	 * O(sqrt(n)) search algorithm for sorted arrays
	 * @param arr		Sorted array to be searched through for target.
	 * @param jump		The intervals to jump over.
	 * @param target	The value to search for.
	 * @return			The index of target within arr, if present. Else -1.
	 */
	public static int jumpSearch(Integer[] arr, int jump, int target) {
		int result = -1;
		
		for (int i = 0, j = 0; i < arr.length; i += jump, j++) {
			if (result != -1) {	//target has already been found
				break;
			}
			System.out.println("Jump " + j + ": " + arr[i]);
			
			if (arr[i] == target) {	//target is encountered by a leap
				result = i;
			} else if (arr[i] > target) {	//target is not encountered by a leap, but a leap encounters a number greater than target
				Integer[] subArr = Arrays.copyOfRange(arr, i - jump + 1, i);
				System.out.println("Beginning Linear Search on " + Arrays.asList(subArr) + " for target " + target);
				result = LinearSearch.linearSearch(subArr, target) + (i - jump + 1);
			}
		}
		if (result == -1) {	//target was not found or passed in any leap, linear search the final elements
			Integer[] subArr = Arrays.copyOfRange(arr, arr.length - ((arr.length - 1) % jump), arr.length);
			System.out.println("Beginning Linear Search on " + Arrays.asList(subArr) + " for target " + target);
			result = LinearSearch.linearSearch(subArr, target) + (arr.length - ((arr.length - 1) % jump));
		}
		
		return result;
	}

}
