package algorithms.search;

import java.util.Arrays;

public class LinearSearch {

	public static void main(String[] args) {
		Integer[] arr = {1, 9, 2, 8, 3, 7, 4, 6, 5};
		Integer target = 7;
		System.out.println("Beginning Linear Search on " + Arrays.asList(arr) + " for target " + target);
		System.out.println(linearSearch(arr, target));
	}
	
	/**
	 * O(n) searching algorithm for an array that may or may not sorted.
	 * @param arr		The array to be searched.
	 * @param target	The target to be found. May or may not be present.
	 * @return			The index of target within arr, if present. Else -1.
	 */
	public static <T> int linearSearch(T[] arr, T target) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println("Step " + (i+1) + ": " + arr[i]);
			if (arr[i].equals(target)) 
				return i;
		}
		return -1;
	}
}
