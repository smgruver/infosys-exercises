package algorithms.search;

import java.util.Arrays;

public class ExponentialSearch {

	public static void main(String[] args) {
		Integer[] arr = {1, 2, 4, 7, 8, 9, 12, 14, 15, 17};
		int target = 13;
		System.out.println("Beginning Exponential Search on " + Arrays.asList(arr) + " for target " + target);
		System.out.println(exponentialSearch(arr, target));
	}

	/**
	 * O(log n) search algorithm for sorted arrays
	 * @param arr		Sorted array to be searched for target
	 * @param target	The value to be searched for in arr
	 * @return			The index of target within arr if present. Else, -1
	 */
	public static int exponentialSearch(Integer[] arr, int target) {
		if (arr[0] == target) {
			return 0;
		} else {
			for (int i = 1; i < arr.length; i *= 2) {
				if (arr[i] == target) {
					return i;
				} else if (arr[i] > target) {
					Integer[] subArr = Arrays.copyOfRange(arr, i/2, i);
					System.out.println("Beginning Binary Search on " + Arrays.asList(subArr) + " for target " + target);
					return BinarySearch.binarySearch(arr, target, i/2, i - 1);
				} else if (i * 2 > arr.length) {
					Integer[] subArr = Arrays.copyOfRange(arr, i, arr.length);
					System.out.println("Beginning Binary Search on " + Arrays.asList(subArr) + " for target " + target);
					return BinarySearch.binarySearch(arr, target, i, arr.length - 1);
				}
			}
		}
		
		return -1;
	}
}
