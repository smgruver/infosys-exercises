package algorithms.search;

import java.util.Arrays;

public class InterpolationSearch {

	public static void main(String[] args) {
		Integer[] arr = {1, 2, 4, 7, 8, 9, 12, 14, 15, 17};
		int target = 10;
		System.out.println("Beginning Interpolation search on " + Arrays.asList(arr) + " for target " + target);
		System.out.println(interpolationSearch(arr, target));
	}

	public static int interpolationSearch(Integer[] arr, int target) {
		int result = -1;
		
		result = interpolationSearchRecursive(arr, 0, arr.length - 1, target);
		
		return result;
	}
	
	/**
	 * O(log(log(n))) search algorithm for a sorted, uniformly distributed array
	 * @param arr		Sorted, uniformly distributed array to be searched
	 * @param low		The lower bound of the array or subarray to be searched
	 * @param high		The upper bound of the array or subarry to be searched
	 * @param target	The value to be searched for
	 * @return			The index of target within arr, if present. Else -1.
	 */
	public static int interpolationSearchRecursive(Integer[] arr, int low, int high, int target) {
		//interpolation formula; finds relative position of target between low and high
		int pos = low + ( (target - arr[low]) * (high - low) / (arr[high] - arr[low]) );
		System.out.println(Arrays.asList(Arrays.copyOfRange(arr, low, high + 1)) + " pos = low+" + (pos - low));
		if (pos < low || pos > high) {
			return -1;
		}
		
		if (arr[pos] == target) { //target found
			return pos;
		} else if (arr[pos] > target) { //target less than current index's value
			return interpolationSearchRecursive(arr, low, pos - 1, target);
		} else {	//arr[pos] < target
			return interpolationSearchRecursive(arr, pos + 1, high, target);
		}
	}
	
}
