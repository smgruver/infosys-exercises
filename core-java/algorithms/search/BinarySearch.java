package algorithms.search;

import java.util.Arrays;

public class BinarySearch {

	public static void main(String[] args) {
		Integer[] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		Integer target = 6;
		System.out.println("Beginning Binary Search on " + Arrays.asList(arr) + " for target " + target);
		System.out.println(binarySearch(arr, target, 0, arr.length - 1));
	}

	/**
	 * O(log n) searching algorithm for a sorted array
	 * @param arr		The array to be searched. Must be in ascending order.
	 * @param target	The target to be found. May or may not be present.
	 * @param low		The lower end of the array or subarray to be searched. Inclusive.
	 * @param high		The higher end of the arry or subarry to be searched. Inclusive.
	 * @return The index of the target in the array, if it is present. Else -1.
	 */
	public static int binarySearch(Integer[] arr, Integer target, int low, int high) {
		int mid = Math.floorDiv(high - low, 2) + low;
		System.out.println(Arrays.asList(Arrays.copyOfRange(arr, low, high + 1)) + " mid: low+" + (mid - low));
		
		if (target.equals(arr[mid])) {
			return mid;	//value found
		} else if (arr[low].equals(arr[high])) {
			return -1;	//value not found in the array
		} else if (target > arr[mid]) {
			return binarySearch(arr, target, mid + 1, high);
		} else if (target < arr[mid]){
			return binarySearch(arr, target, low, mid - 1);
		} else {
			return -1;
		}
	}
}
