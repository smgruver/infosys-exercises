package ioOperations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
 * In a folder two text files, Input1.txt and Input2.txt are present containing data.
	Write a Java program that creates an output file Output.txt which has content of both Input1.txt and Input2.txt.
 */
public class fileOperations {

	public static void main(String[] args) {
		File input1 = new File("src/ioOperations/input1.txt");
		File input2 = new File("src/ioOperations/input2.txt");
		File output = new File("src/ioOperations/output.txt");
		
		String result = "";
		
		try (RandomAccessFile raf1 = new RandomAccessFile(input1, "r");) {
			String nextLine = raf1.readLine();
			while (nextLine != null) {
				result += nextLine + '\n';
				nextLine = raf1.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result += '\n';
		
		try (RandomAccessFile raf2 = new RandomAccessFile(input2, "r");) {
			String nextLine = raf2.readLine();
			while (nextLine != null) {
				result += nextLine + '\n';
				nextLine = raf2.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (RandomAccessFile raf3 = new RandomAccessFile(output, "rw");) {
			raf3.write(result.getBytes());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
