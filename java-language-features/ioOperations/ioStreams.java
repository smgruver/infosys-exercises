package ioOperations;

import java.io.*;

/*
 * For a student John there is a text file named JohnTerm1.txt containing marks for three subjects English, Maths, Science for Term 1. 
 * Similarly, for term2 there is another text file JohnTerm2.txt. 
 * Read these two files and sum the two terms marks for each subject and write to a file JohnTotal.txt for the complete year.
 */
public class ioStreams {

	public static void main(String[] args) {
		int englishTotal = 0, mathTotal = 0, scienceTotal = 0;
		
		try (BufferedReader fin = new BufferedReader(new FileReader("src/ioOperations/JohnTerm1.txt"));){
			 englishTotal += Integer.parseInt(fin.readLine().trim());
			 mathTotal += Integer.parseInt(fin.readLine().trim());
			 scienceTotal += Integer.parseInt(fin.readLine().trim());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		} 
		 System.out.println(englishTotal + " " + mathTotal + " " + scienceTotal);
		
		String str = "";
		try (BufferedReader fin = new BufferedReader(new FileReader("src/ioOperations/JohnTerm2.txt"));){
			 int i = fin.read();
			 while (i != -1) {
				 char ch = (char)i;
				 str += ch;
				 i = fin.read();
			 }
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		} 
		 String[] strArr = str.split("\\s+");
		 englishTotal += Integer.parseInt(strArr[0]);
		 mathTotal += Integer.parseInt(strArr[1]);
		 scienceTotal += Integer.parseInt(strArr[2]);
		 System.out.println(englishTotal + " " + mathTotal + " " + scienceTotal);
		
		try (BufferedWriter fout = new BufferedWriter(new FileWriter("src/ioOperations/JohnTotal.txt"))){
			 fout.write(new Integer(englishTotal).toString());
			 fout.write('\n');
			 fout.write(new Integer(mathTotal).toString());
			 fout.write('\n');
			 fout.write(new Integer(scienceTotal).toString());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
}
