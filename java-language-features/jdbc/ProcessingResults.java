package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProcessingResults {
	private static final String DB_PATH = ".\\db-test";
	private static final String CONNECTION_STRING = String.format("jdbc:h2:%s", DB_PATH);
	private static final String JDBC_DRIVER_NAME = "org.h2.Driver";
	private static final String USERNAME = "sa";
	private static final String PASSWORD = "";
	
	public static Connection currentConnection = null;
	
	public static int passingGrade = 60;

	public static void main(String[] args) throws SQLException {
		
//		create();
//		insertPart1();
		updateRollback();
		
		ResultSet results = select();
		display(results);

//		delete();
	}
	
	//Step 2: Connect to database
	public static Connection connect() {
		
		if (currentConnection == null) {
			try {
				Class.forName(JDBC_DRIVER_NAME);
				currentConnection = DriverManager.getConnection(CONNECTION_STRING, USERNAME, PASSWORD);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		return currentConnection;
	}
	
	public static void create() {
		//Step 1: Load driver
		try {
			//Step 3.1: Create a statement
			Statement init = connect().createStatement();
			//Step 3.2: Execute the statement
			init.executeUpdate("create table Student(Name varchar(24),RollNumber int, CourseName varchar(24), Score int)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Successfully initiated database");
	}
	
	public static void delete() {
		try {
			Statement delete = connect().createStatement();
			delete.executeUpdate("drop table Student;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void insertPart1() {
		PreparedStatement insert;
		boolean success;
		try {
			insert = connect().prepareStatement("insert into Student (Name, RollNumber, CourseName, Score) values (?, ?, ?, ?);");
			insert.setString(1, "John");
			insert.setInt(2, 1);
			insert.setString(3, "English");
			insert.setInt(4, 100);
			insert.execute();

				insert.setString(1, "Jane");
				insert.setInt(2, 2);
				insert.setString(3, "Math");
				insert.setInt(4, 95);
				insert.execute();

				insert.setString(1, "Doe");
				insert.setInt(2, 3);
				insert.setString(3, "Science");
				insert.setInt(4, 0);
				insert.execute();

				insert.setString(1, "Doen't");
				insert.setInt(2, 4);
				insert.setString(3, "Gym");
				insert.setInt(4, 40);
				insert.execute();
				
				insert.setString(1, "Doen't've");
				insert.setInt(2, 3);
				insert.setString(3, "Lunch");
				insert.setInt(4, 2);
				insert.execute();
				success = true;
		} catch (SQLException e) {
			success = false;
			e.printStackTrace();
		}
	}
	
	public static void updateRollback() throws SQLException{
		Connection conn = connect();
		PreparedStatement update;
		conn.setAutoCommit(false);
		try {
			update = conn.prepareStatement("update Student set Score = 60 where Name = 'John';");
			update.execute();
			
			if (true) throw new SQLException("The row should not update because I'm throwing this exception. John's score will stay 100.");
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}
	}
	
	public static ResultSet select() {
		PreparedStatement select;
		ResultSet results = null;
		try {
			select = connect().prepareStatement("select * from Student where Score >= ?;");
			select.setInt(1, passingGrade);
			results = select.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}
	
	public static void display(ResultSet results) {
		try {
			if (results.isBeforeFirst()) {
				while (results.next()) {
					System.out.println(results.getString("Name") + " " + results.getInt("RollNumber") + " " + results.getString("CourseName") + " " + results.getInt("Score"));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("ResultSet object not initialized");
			e.printStackTrace();
		}
	}

}
