package multithreading.synchonization;

public class DataContainer {
   private int intData = -1;
	   
   public int get() {		        
            return intData;	      
   }

   public void put(int value) {
	      intData = value;
   }
	   
   public static void main(String[] args) {
	   DataContainer dc = new DataContainer();
	   Producer p1 = new Producer(dc, 1);
	   Consumer c = new Consumer(dc);
	   
	   new Thread(c).start();
	   
	   Thread P1 = new Thread(p1);
	   P1.start();
	   try {
		P1.join();
	   } catch (InterruptedException e) {
		   Thread.currentThread().interrupt();
		   e.printStackTrace();
	   }
	   new Thread(c).start();
	}
}
