package multithreading.synchonization;

public class Producer implements Runnable {

	private DataContainer dc = null;
	private int data;
	
	public Producer(DataContainer dc, int toPut) {
		this.dc = dc;
		this.data = toPut;
	}
	
	@Override
	public void run() {
		synchronized(dc) {
			dc.put(data);			
		}
	}
}
