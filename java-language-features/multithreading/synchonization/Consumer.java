package multithreading.synchonization;

public class Consumer implements Runnable{

	private DataContainer dc = null;
	
	public Consumer(DataContainer dc) {
		this.dc = dc;
	}
	
	@Override
	public void run() {
		synchronized(dc) {
			System.out.println(dc.get());			
		}
	}

}
