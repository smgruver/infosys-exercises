package multithreading;

public class ThreadSubclass extends Thread{

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			new ThreadSubclass().start();
		}

	}
	
	@Override
	public void run() {
		System.out.println("Hello, threads");
	}

}
