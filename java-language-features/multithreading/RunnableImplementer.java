package multithreading;

public class RunnableImplementer implements Runnable {
	
	private String name;
	
	public RunnableImplementer(String name){
		this.name = name;
	}

	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			Thread t = new Thread(new RunnableImplementer("runnable" + i), "runnable" + i);
			t.setPriority(i);
			t.start();
		}
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Hello, threads from " + name);
	}

}
