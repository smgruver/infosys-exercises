package serialization;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/*
 * Write a Java program that creates Term1 and Term2 objects by reading the input files of 'Buffering and try-catch variant - Exercise'. 
 * Do the required changes to Term and Mark classes and serialize these term objects to JohnTerm1.dat and JohnTerm2.dat respectively.
 */
public class SerializationExerciseInit {

	public static void main(String[] args) {
		Term term1 = new Term();
		term1.setStudentId("000001");
		term1.setTermName("fall 2020");
		term1.setTermMarks(new ArrayList<Mark>());
		Term term2 = new Term();
		term2.setStudentId("000001");
		term2.setTermName("spring 2020");
		term2.setTermMarks(new ArrayList<Mark>());
		
		int englishMark, mathMark, scienceMark;
		
		try (BufferedReader fin = new BufferedReader(new FileReader("src/ioOperations/JohnTerm1.txt"));){
			 englishMark = Integer.parseInt(fin.readLine().trim());
			 Mark fallEnglish = new Mark();
			 fallEnglish.setSubject("English");
			 fallEnglish.setMarks(englishMark);
			 term1.getTermMarks().add(fallEnglish);
			 term1.setTermMarks(term1.getTermMarks());
			 
			 mathMark = Integer.parseInt(fin.readLine().trim());
			 Mark fallMath = new Mark();
			 fallMath.setSubject("Math");
			 fallMath.setMarks(mathMark);
			 term1.getTermMarks().add(fallMath);
			 term1.setTermMarks(term1.getTermMarks());
			 
			 scienceMark = Integer.parseInt(fin.readLine().trim());
			 Mark fallScience = new Mark();
			 fallScience.setSubject("Science");
			 fallScience.setMarks(scienceMark);
			 term1.getTermMarks().add(fallScience);
			 term1.setTermMarks(term1.getTermMarks());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		} 
		 System.out.println(term1);
		
		String str = "";
		try (BufferedReader fin = new BufferedReader(new FileReader("src/ioOperations/JohnTerm2.txt"));){
			 int i = fin.read();
			 while (i != -1) {
				 char ch = (char)i;
				 str += ch;
				 i = fin.read();
			 }
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		} 
		 String[] strArr = str.split("\\s+");
		 
		 englishMark = Integer.parseInt(strArr[0]);
		 Mark springEnglish = new Mark();
		 springEnglish.setSubject("English");
		 springEnglish.setMarks(englishMark);
		 term2.getTermMarks().add(springEnglish);
		 term2.setTermMarks(term2.getTermMarks());
		 
		 mathMark = Integer.parseInt(strArr[1]);
		 Mark springMath = new Mark();
		 springMath.setSubject("Math");
		 springMath.setMarks(englishMark);
		 term2.getTermMarks().add(springMath);
		 term2.setTermMarks(term2.getTermMarks());
		 
		 scienceMark = Integer.parseInt(strArr[2]);
		 Mark springScience = new Mark();
		 springScience.setSubject("Science");
		 springScience.setMarks(scienceMark);
		 term2.getTermMarks().add(springScience);
		 term2.setTermMarks(term2.getTermMarks());
		 
		 System.out.println(term2);
		 
		try (ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream("src/serialization/JohnTerm1.dat"));){
			objOut.writeObject(term1);
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		try (ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream("src/serialization/JohnTerm2.dat"));){
			objOut.writeObject(term2);
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
	

}
