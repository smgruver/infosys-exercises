package serialization;

import java.io.Serializable;

public class Mark implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String subject;
	private double mark;
	
    public String getSubject() {
		return subject;
	}
        
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public double getMark() {
		return mark;
	}
	
	public void setMarks(double mark) {
		this.mark = mark;
	}

	@Override
	public String toString() {
		return "Mark [subject=" + subject + ", mark=" + mark + "]";
	}
	
	

}
