package serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/*
 * Now write another Java Program that reads the JohnTerm1.dat and JohnTerm2.dat into objects of type Term. 
 * Calculate the total term marks based on these objects and print the marks on the console.
 */
public class SerializationExerciseUtilization {

	public static void main(String[] args) {
		Term term1;
		Term term2;
		double engTotal = 0, matTotal = 0, sciTotal = 0;
		
		try (ObjectInputStream objIn = new ObjectInputStream(new FileInputStream("src/serialization/JohnTerm1.dat"));) {
			term1 = (Term)objIn.readObject();
			System.out.println(term1);
			engTotal += term1.getTermMarks().get(0).getMark();
			matTotal += term1.getTermMarks().get(1).getMark();
			sciTotal += term1.getTermMarks().get(2).getMark();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (ObjectInputStream objIn = new ObjectInputStream(new FileInputStream("src/serialization/JohnTerm2.dat"));) {
			term2 = (Term)objIn.readObject();
			System.out.println(term2);
			engTotal += term2.getTermMarks().get(0).getMark();
			matTotal += term2.getTermMarks().get(1).getMark();
			sciTotal += term2.getTermMarks().get(2).getMark();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(engTotal + " " + matTotal + " " + sciTotal);
	}

}
