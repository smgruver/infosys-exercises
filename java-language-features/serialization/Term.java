package serialization;

import java.io.Serializable;
import java.util.List;

public class Term implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String termName;
	private String studentId;
	private List<Mark> termMarks;

	public String getTermName() {
		return termName;
	}
	public void setTermName(String termName) {
		this.termName = termName;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public List<Mark> getTermMarks() {
		return termMarks;
	}
	public void setTermMarks(List<Mark> termMarks) {
		this.termMarks = termMarks;
	}
	@Override
	public String toString() {
		return "Term [termName=" + termName + ", studentId=" + studentId + ", termMarks=" + termMarks + "]";
	}
}
