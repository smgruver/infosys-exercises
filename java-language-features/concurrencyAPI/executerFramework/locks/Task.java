package concurrencyAPI.executerFramework.locks;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantLock;

public class Task implements Callable<Integer>{
	ReentrantLock lock;
	private static int counter;
	
	public Task(ReentrantLock lock) {
		this.lock = lock;
	}
	
	private int incrementCtr() {
		return ++counter;
	}
	
	@Override
	public Integer call() {
		try {
			lock.lock();
			Thread.sleep(1000);
			incrementCtr();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
		return counter;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

}
