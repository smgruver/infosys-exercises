package concurrencyAPI.executerFramework.threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskRunner {

	public static void main(String[] args) {
		Task t1 = new Task(4);
		Task t2 = new Task(7);
		Task[] tasks = {t1, t2};
		
		ExecutorService ex = Executors.newFixedThreadPool(1);
		
		for (int i = 0; i < tasks.length; i++) {
			ex.execute(tasks[i]);
		}
		ex.shutdown();
		while (!ex.isTerminated()) {}
		System.out.println("All tasks complete");
	}
}
