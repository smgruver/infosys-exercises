package concurrencyAPI.executerFramework.threadPool;

public class Task implements Runnable {

	private int counter;
	
	public Task(int data) {
		this.counter = data;
	}
	
	private int incrementCtr() {
		return ++counter;
	}
	
	@Override
	public void run() {
		System.out.println(incrementCtr());

	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

}
