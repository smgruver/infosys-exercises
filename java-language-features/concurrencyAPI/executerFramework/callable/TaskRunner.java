package concurrencyAPI.executerFramework.callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TaskRunner {

	public static void main(String[] args) {
		Task t1 = new Task(4);
		Task t2 = new Task(7);
		Task[] tasks = {t1, t2};
		ExecutorService ex = Executors.newFixedThreadPool(1);
		
		for (Task t : tasks) {
			Future<Integer> f = ex.submit(t);
			try {
				System.out.println(f.get());
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ex.shutdown();
		while (!ex.isTerminated()) {}
		System.out.println("All tasks complete");
	}
}
