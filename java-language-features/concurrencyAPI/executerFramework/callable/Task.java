package concurrencyAPI.executerFramework.callable;

import java.util.concurrent.Callable;

public class Task implements Callable<Integer> {

	private int counter;
	
	public Task(int data) {
		this.counter = data;
	}
	
	private int incrementCtr() {
		return ++counter;
	}
	
	@Override
	public Integer call() {
		try {
			Thread.sleep(1000);
			incrementCtr();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return counter;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

}
