package javapractice.basic.medium;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.TreeSet;

class Student implements Comparable<Student>{
	int id;
	String name;
	double cgpa;
	
	Student(int id, String name, double cgpa){
		this.id = id;
		this.name = name;
		this.cgpa = cgpa;
	}
	
	int getID() {
		return this.id;
	}
	
	String getName() {
		return this.name;
	}
	
	double getCGPA(){
		return this.cgpa;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", cgpa=" + cgpa + "]";
	}

	@Override
	public int compareTo(Student s2) {
		if (this.getCGPA() != s2.getCGPA()) {
			if (this.getCGPA() > s2.getCGPA())
				return -1;
			else
				return 1;
		} else if (!this.getName().equals(s2.getName())) {
			return this.getName().compareTo(s2.getName());
		} else {
			return s2.getID() - this.getID();
		}
	}
	
}
class Priorities{
	List<Student> getStudents(List<String> events){
		PriorityQueue<Student> pq = new PriorityQueue<>();
		
		for (String input : events) {
			if (input.startsWith("SERVED")) {
				pq.poll();
			} else {
				String[] operands = input.split(" ");
				pq.add(new Student(Integer.parseInt(operands[3]), operands[1], Double.parseDouble(operands[2])));
			}
//			System.out.println(pq);
		}
		
		return new ArrayList<Student>(new TreeSet<Student>(pq));
	}
}
public class StudentPriorityQueue {
	private final static Scanner scan = new Scanner(System.in);
    private final static Priorities priorities = new Priorities();
    
    public static void main(String[] args) {
        int totalEvents = Integer.parseInt(scan.nextLine());    
        List<String> events = new ArrayList<>();
        
        while (totalEvents-- != 0) {
            String event = scan.nextLine();
            events.add(event);
        }
        
        List<Student> students = priorities.getStudents(events);
        
        if (students.isEmpty()) {
            System.out.println("EMPTY");
        } else {
            for (Student st: students) {
                System.out.println(st.getName());
            }
        }
    }
}
