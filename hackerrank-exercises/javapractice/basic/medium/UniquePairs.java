package javapractice.basic.medium;

import java.util.*;

public class UniquePairs {

	public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        String [] pair_left = new String[t];
        String [] pair_right = new String[t];
        
        for (int i = 0; i < t; i++) {
            pair_left[i] = s.next();
            pair_right[i] = s.next();
        }

        //Write your code here
        HashSet<String> uniques = new HashSet<>();
        int result = 0;
        for (int i = 0; i < t; i++) {
        	String key = (pair_left[i].concat(pair_right[i])).concat(pair_right[i].concat(pair_left[i]));
        	
        	if (uniques.add(key)) {
        		result++;
        	}
        	System.out.println(result);
        }
   }
}
