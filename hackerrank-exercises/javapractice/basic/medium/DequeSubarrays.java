package javapractice.basic.medium;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
 * https://www.hackerrank.com/challenges/java-dequeue/problem
 */
public class DequeSubarrays {
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque<Integer>();
        int n = in.nextInt();
        int m = in.nextInt();
        
        //My code
        int currentUniques = 0;
        int maxUniques = 0;
        HashMap<Integer, Integer> map = new HashMap<>();

        //First m elements
        for (int i = 0; i < m; i++) {
        	int next = in.nextInt();
        	deque.addFirst(next);
        	if (map.get(next) == null) {
        		map.put(next, 0);
        	}
        	map.put(next, map.get(next) + 1);
        }
        currentUniques = map.keySet().size();
        maxUniques = currentUniques;
//        System.out.println("Map: " + map);
//        System.out.println("Uniques: " + map.keySet().stream().filter(key -> map.get(key) > 0).collect(Collectors.toSet()));
        
        //After the first m elements, we start removing elements from the deque
        for (int i = m; i < n; i++) {
            int next = in.nextInt();
            deque.addFirst(next);
            int last = deque.removeLast();
            
            if (next != last) {
            	Integer mapLast = map.get(last);
                if (mapLast != null) {	//Just in case
                	map.put(last, mapLast - 1);	//Decrement value at map(last) to represent the count within the current subarray decrementing
                	if (mapLast - 1 <= 0) {	//If there are no more of last's value within the subarray, decrement currentUniques
                		currentUniques--;
                	}
                }
                
                Integer mapNext = map.get(next);
                if (mapNext == null) {	//First instance of this value within the array
            		map.put(next, 0);
            	}
            	map.put(next, map.get(next) + 1);
            	if (map.get(next) == 1) {	//First instance of this value within the subarray
            		currentUniques++;
            	}
            	
            	if (currentUniques > maxUniques) {
            		maxUniques = currentUniques;
            	}
            }
//            System.out.println("Map: " + map);
//            System.out.println("Uniques: " + map.keySet().stream().filter(key -> map.get(key) > 0).collect(Collectors.toSet()));
        }
        System.out.println(maxUniques);
        //End of my code
    }
}
