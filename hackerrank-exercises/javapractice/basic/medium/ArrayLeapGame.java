package javapractice.basic.medium;

import java.util.*;

//https://www.hackerrank.com/challenges/java-1d-array/problem
public class ArrayLeapGame {

    public static boolean canWin(int leap, int[] game) {
    	boolean[] visited = new boolean[game.length];
    	for (int i = 0; i < game.length; i++) {
    		if (game[i] == 1) {
    			visited[i] = true;
    		}
    	}
    	
        return gameDFS(leap, 0, game, new boolean[game.length]);
    }
    
    public static boolean gameDFS(int leap, int index, int[] game, boolean[] visited) {
//    	System.out.println("Calling gameDFS with index " + index);
    	
    	if (index > game.length - 1) {
//    		System.out.println("Found end, returning true");
    		return true;
    	} else if (index < 0 || game[index] == 1 || visited[index] == true){
    		return false;
    	} else {
//    		System.out.println("game value = " + game[index] + ", visited value = " + visited[index]);
    		//ensure no infinite loops
    		visited [index] = true;
    		
    		boolean stepForward = gameDFS(leap, index + 1, game, visited);
    		boolean leapForward = gameDFS(leap, index + leap, game, visited);
    		boolean stepBackward = gameDFS(leap, index - 1, game, visited);
    		
    		return (stepForward || leapForward || stepBackward);
    	}
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();
            
            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
    }
}
