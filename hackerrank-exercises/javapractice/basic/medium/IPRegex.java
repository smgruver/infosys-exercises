package javapractice.basic.medium;

import java.util.Scanner;

class IPRegex{

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }

    }
}

//Write your code here
class MyRegex {
	String pattern = "^((2[0-5]{0,2}|24[0-9]?|[0-1]?[0-9]{1,2})\\.){3}" + 
	        "(2[0-5]{0,2}|24[0-9]?|[0-1]?[0-9]{1,2})$";
}