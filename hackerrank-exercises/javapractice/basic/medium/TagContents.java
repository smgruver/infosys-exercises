package javapractice.basic.medium;

import java.util.*;

/*
 * In a tag-based language like XML or HTML, contents are enclosed between a start tag and an end tag like <tag>contents</tag>. 
 * Note that the corresponding end tag starts with a /.
 * 
 * Given a string of text in a tag-based language, 
 * 		parse this text and retrieve the contents enclosed within sequences of well-organized tags meeting the following criterion:
 * 			The name of the start and end tags must be same. 
 * 
 * 			The HTML code <h1>Hello World</h2> is not valid, 
 * 			because the text starts with an h1 tag and ends with a non-matching h2 tag.
 * 
 * 			Tags can be nested, but content between nested tags is considered not valid. 
 * 			For example, in <h1><a>contents</a>invalid</h1>, contents is valid but invalid is not valid.
 * 
 * 			Tags can consist of any printable characters.
 */
public class TagContents{
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
		while(testCases>=0){
			String line = in.nextLine();
			
			List<String> result = contentExtractor(line);
          	if (result.isEmpty()) {
          		System.out.println("None");
          	} else {
          		for (String s : result) {
          			System.out.println(s);
          		}
          	}
			
			testCases--;
		}
	}
	
	public static List<String> contentExtractor(String input) {
		List<String> contents = new ArrayList<>();
		
		System.out.println("Extracting content from: " + input);
		
		//trim to only contents within tags
		//obtain first tag
			//if first tag is a closing tag, 
				//trim it
			//else, 
				//obtain corresponding closing tag
					//if the closing tag exists,
						//trim the opening and closing tag
						//if the result of the above operations contains a tag, 
							//repeat the above operations
						//else
							//save the result as content
					//else
						//No content
		
		//Hone in on the first starting tag and its corresponding closing tag
		int openingStart = input.indexOf('<');
		
		int openingEnd = input.indexOf('>', openingStart);
		if (openingEnd == -1) {
			//The opening tag never ends
			contents.add("None");
			return contents;
		}
		
		String openingTag = input.substring(openingStart + 1, openingEnd);
		System.out.println("Opening tag: " + openingTag + " at index " + openingStart);
		if (openingTag.length() == 0 || openingTag.startsWith("/")) {
			//The opening tag has no label or is actually a closing tag
			return contentExtractor(input.substring(openingEnd + 1));
		}
		
		int closingStart = input.lastIndexOf("</" + openingTag + ">");
		System.out.println("Matching closing tag found at index " + closingStart);
		if (closingStart == -1) {
			//Opening tag has no corresponding closing tag
			return contentExtractor(input.substring(openingEnd + 1));
		}
		
		String innerString = input.substring(openingEnd + 1, closingStart);
		System.out.println("Content found: " + innerString);
		if (innerString.matches(".*<.+>.*|.*<\\/.+>.*")) {
			//inner string also needs to be unwrapped
			contents.addAll(contentExtractor(innerString));
		} else if (innerString.length() != 0){
			contents.add(innerString);
		}
		
		if (closingStart + openingTag.length() + 3 < input.length() - 1) {
			//if the closing tag is not the end of the input
			System.out.println("Remaining content found");
			contents.addAll(contentExtractor(input.substring(closingStart + openingTag.length() + 3)));
		}
		
		HashSet<String> toRemove = new HashSet<String>();
		toRemove.add("None");
		contents.removeAll(toRemove);
		
		return contents;
	}
}




