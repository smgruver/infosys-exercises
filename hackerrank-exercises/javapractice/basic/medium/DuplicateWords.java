package javapractice.basic.medium;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * https://www.hackerrank.com/challenges/duplicate-word/problem
 * Removes adjacent repeated words (case-insensitive)
 */
public class DuplicateWords {
	public static void main(String[] args) {

        String regex = "\\b(\\w+)(?:\\W+\\1\\b)+";	//wrote regex
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);	//included necessary flag

        Scanner in = new Scanner(System.in);
        int numSentences = Integer.parseInt(in.nextLine());
        
        while (numSentences-- > 0) {
            String input = in.nextLine();
            
            Matcher m = p.matcher(input);
            
            // Check for subsequences of input that match the compiled pattern
            while (m.find()) {	//While matches are found, AKA while repeated words are present
                input = input.replaceAll(m.group(), m.group(1));	//wrote statement
                //m.group() is the entire matching substring
                /*
                 * m.group(1) is the first capture group, which is the first instance of any given word
                 * This is because of the layout of the regex: \b(\w+)(?:\W+\1\b)+
                 * The first capture group is the (\w+), the first word seen in the match (the first word with a repeat)
                 * The next capture group finds the content of the first capture group "\1" preceded by any number of words "\W+" 
                 * 		and followed by the end of a word "\b". Any number of repeats are found ")+"
                 * The "?:" means that the results of the second parentheses won't be saved to a capture group, only done to save time
                 */
            }
            
            // Prints the modified sentence.
            System.out.println(input);
        }
        
        in.close();
    }
}
