package javapractice.basic.easy;

import java.util.BitSet;
import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-bitset/problem
 */
public class BitSetOperations {

	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT.*/
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		BitSet[] b = {null, new BitSet(n), new BitSet(n)};
		
		for (int i = 0; i < m; i++) {
			String operation = in.next();
			int operand1 = in.nextInt();
			int operand2 = in.nextInt();
			
			switch(operation) {
			case "AND":
				b[operand1].and(b[operand2]);
				break;
			case "OR":
				b[operand1].or(b[operand2]);
				break;
			case "XOR":
				b[operand1].xor(b[operand2]);
				break;
			case "SET":
				b[operand1].set(operand2);
				break;
			case "FLIP":
				b[operand1].flip(operand2);
				break;
			default:
			}
			
			System.out.println(b[1].cardinality() + " " + b[2].cardinality());
		}
    }

}
