package javapractice.basic.easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-string-tokens/problem
 */
public class StringTokens {

	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        
        //My code
        List<String> tokens = new ArrayList(Arrays.asList(s.split("[\\s+!,\\?\\._@']+")));
        tokens.remove("");
        System.out.println(tokens.size());
        for (String token : tokens) {
        	System.out.println(token);
        }
        //End of my code
        
        scan.close();
    }
}

