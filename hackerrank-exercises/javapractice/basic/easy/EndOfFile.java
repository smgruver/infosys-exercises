package javapractice.basic.easy;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-end-of-file/problem
 */
public class EndOfFile {
	public static void main(String[] args) {
		//My code
        Scanner in = new Scanner(System.in);
		String input;
		long counter = 0;
        
		while(in.hasNextLine()) {
        	input = in.nextLine();
        	counter++;
        	System.out.printf("%d %s%n", counter, input);
        }
		//End of my code
    }
}
