package javapractice.basic.easy;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-loops/problem
 */
public class LoopsII {
	public static void main(String []argh){
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        for(int i=0;i<t;i++){
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            
            //My code
            int previous = a;
            for (int j = 0; j < n; j++) {
            	previous = previous + (int)Math.pow(2, j) * b;
            	System.out.printf("%d ", previous);
            	
            }
            //End of my code
        }
        in.close();
    }
}
