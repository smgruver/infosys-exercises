package javapractice.basic.easy;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-static-initializer-block/problem
 */
public class StaticInitializerBlock {

	//My code
	static int B = 0;
	static int H = 0;
	static boolean flag = false;
	static Scanner in = new Scanner(System.in);
	static {
		B = in.nextInt();
		H = in.nextInt();
		
		try {
			if (B <= 0 || H <= 0) {
				throw new Exception("Breadth and height must be positive");
			} else {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	//End of my code

	public static void main(String[] args){
		if(flag){
				int area=B*H;
				System.out.print(area);
		}
		
	}//end of main
}
