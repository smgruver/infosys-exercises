package javapractice.basic.easy;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/java-currency-formatter/problem
 */
public class CurrencyFormatter {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();
        
        //My code
        NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US);
        NumberFormat india = NumberFormat.getCurrencyInstance(new Locale("en", "IN")); //Creates a new locale with language: english and country: india
        NumberFormat china = NumberFormat.getCurrencyInstance(Locale.CHINA);
        NumberFormat france = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        //End of my code
        
        //edited these from [variable name] to [variable name].format(payment)
        System.out.println("US: " + us.format(payment));
        System.out.println("India: " + india.format(payment));
        System.out.println("China: " + china.format(payment));
        System.out.println("France: " + france.format(payment));
    }
}
