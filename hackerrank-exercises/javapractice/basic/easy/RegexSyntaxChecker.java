package javapractice.basic.easy;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegexSyntaxChecker {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
		while(in.hasNext()){
			String pattern = in.nextLine();
			//My code
          	try {
          		Pattern.compile(pattern);
          		System.out.println("Valid");
          	} catch (PatternSyntaxException e) {
          		System.out.println("Invalid");
          	} catch (Exception e) {
          		System.out.println("Invalid");
          	}
          	//End of my code
		}
	}
}
