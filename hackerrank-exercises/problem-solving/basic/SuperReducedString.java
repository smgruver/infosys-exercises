package problemSolving.basic;

import java.io.*;
import java.util.*;
import java.util.stream.*;

/*
 * Repeatedly reduce a string to its minimum possible length by removing adjacent characters of the same value.
 * If the result is an empty string, return "Empty String"
 * Ex: aabbbccd -> bbbccd -> bccd -> bd
 */

public class SuperReducedString {

    // Complete the superReducedString function below.
    static String superReducedString(String s) {
        Queue<Character> chars = new LinkedList<Character>();
        StringBuilder result = new StringBuilder();
        boolean didChange = false;
        //Adding char array of s into queue
        IntStream stream = s.chars();
        stream.forEach(ch -> chars.add((char)ch));
        
        while (!chars.isEmpty()) {
        	Character thisChar = chars.poll();
        	
        	if (thisChar.equals(chars.peek())) {
        		chars.poll();
        		didChange = true;
        		continue;
        	} else {
        		result.append(thisChar);
        	}
        }
        
        if (result.length() == 0) {
        	return "Empty String";
        } else {
        	if (didChange) {
        		return superReducedString(result.toString());
        	} else {
        		return result.toString();
        	}
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String s = bufferedReader.readLine();

        String result = superReducedString(s);

        System.out.println(result);

        bufferedReader.close();
    }
}
