package problemSolving.basic;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


/*
 * Louise joined a social networking site to stay in touch with her friends. 
 * The signup page required her to input a name and a password. 
 * However, the password must be strong. 
 * The website considers a password to be strong if it satisfies the following criteria:

	Its length is at least .
	It contains at least one digit.
	It contains at least one lowercase English character.
	It contains at least one uppercase English character.
	It contains at least one special character. The special characters are: !@#$%^&*()-+

	She typed a random string of length n in the password field but wasn't sure if it was strong. 
	Given the string she typed, can you find the minimum number of characters she must add to make her password strong?
 */
public class StrongPassword {

    // Complete the minimumNumber function below.
    static int minimumNumber(int n, String password) {
    	boolean atLeast6 = (password.length() >= 6);
    	boolean lowerCasePresent = password.matches(".*[a-z].*");
    	boolean upperCasePresent = password.matches(".*[A-Z].*");
    	boolean digitPresent = password.matches(".*\\d.*");;
    	boolean specialPresent = password.matches(".*[\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\-\\+].*");
    	boolean[] checks = {lowerCasePresent, upperCasePresent, digitPresent, specialPresent};
    	
    	int missingChars = 0;
    	for (boolean b : checks) {
    		if (!b)
    			missingChars++;
    	}
    	
    	System.out.println(atLeast6 + " " + lowerCasePresent + " " + upperCasePresent + " " + digitPresent + " " + specialPresent);
    	
    	return (atLeast6 ? missingChars : Math.max(missingChars, 6 - password.length()));
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String password = scanner.nextLine();

        int answer = minimumNumber(n, password);

        System.out.println(answer);

        scanner.close();
    }
}
