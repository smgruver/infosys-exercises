package problemSolving.basic.datastructures;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/*
 * https://www.hackerrank.com/challenges/dynamic-array/problem
 */
class DynamicArrayResult {

    /*
     * Complete the 'dynamicArray' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. 2D_INTEGER_ARRAY queries
     */

    public static List<Integer> dynamicArray(int n, List<List<Integer>> queries) {
    // Write your code here
    	Scanner in = new Scanner(System.in);
    	Integer lastAnswer = 0;
    	List<List<Integer>> seqList = new ArrayList<List<Integer>>();
    	List<Integer> result = new ArrayList<Integer>();
    	for (int i = 0; i < n; i++) {
    		seqList.add(new ArrayList<Integer>());
    	}
    	
    	for (List<Integer> input : queries) {
    		int queryType = input.get(0);
    		int x = input.get(1);
    		int y = input.get(2);
    		switch(queryType) {
    		case 1:
    			seqList.get((x ^ lastAnswer) % n).add(y);
    			break;
    		case 2:
    			int seqIndex = (x ^ lastAnswer) % n;
    			lastAnswer = seqList.get(seqIndex).get(y % (seqList.get(seqIndex).size()));
    			result.add(lastAnswer);
    			break;
    		default:
    			break;
    		}
    	}
    	return result;
    }

}

public class DynamicArray {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int q = Integer.parseInt(firstMultipleInput[1]);

        List<List<Integer>> queries = new ArrayList<>();

        IntStream.range(0, q).forEach(i -> {
            try {
                queries.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        List<Integer> result = DynamicArrayResult.dynamicArray(n, queries);

        bufferedWriter.write(
            result.stream()
                .map(Object::toString)
                .collect(joining("\n"))
            + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}

