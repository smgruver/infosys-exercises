package problemSolving.basic.datastructures;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

/* https://www.hackerrank.com/challenges/crush/problem
 * 
 */
public class ArrayManipulation {

	//O(N + 1) + O(2*K) + O(N) = O(2N + 2K)
    // Complete the arrayManipulation function below.
	static long arrayManipulation(int n, int[][] queries) {
    	long[] nums = new long[n + 1];
    	for (long l : nums) {
    		l = 0;
    	}
    	
    	int maxIndex = queries[0][0];
    	for (int[] query : queries) {
    		nums[query[0]] += query[2]; //Increase at the beginning of the range
    		if (query[1] < n) {
    			nums[query[1] + 1] -= query[2];	//Decrease at the end of the range
    		}
    	}
    	
    	long max = nums[1];
    	long sum = nums[1];
    	for (int i = 2; i <= n; i++) {	//Add to sum when entering a range of addition, subtract when exiting
    		sum += nums[i];
    		if (sum > max) {
    			max = sum;
    		}
    	}
    	return max;
    }
	
	//O(N+1) + O(K * N) + O(N) = O(3N*K)
	static long arrayManipulationTooSlow(int n, int[][] queries) {
    	long[] nums = new long[n + 1];
    	for (long l : nums) {
    		l = 0;
    	}
    	
    	int maxIndex = queries[0][0];
    	for (int[] query : queries) {
    		for (int i = query[0]; i <= query[1]; i++) {
    			nums[i] += query[2];
    		}
    	}
    	
    	long max = nums[1];
    	for (int i = 1; i <= n; i++) {
    		if (nums[i] > max) {
    			max = nums[i];
    		}
    	}
    	return max;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] nm = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nm[0]);

        int m = Integer.parseInt(nm[1]);

        int[][] queries = new int[m][3];

        for (int i = 0; i < m; i++) {
            String[] queriesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 3; j++) {
                int queriesItem = Integer.parseInt(queriesRowItems[j]);
                queries[i][j] = queriesItem;
            }
        }

        long result = arrayManipulation(n, queries);

        System.out.println(result);

        scanner.close();
    }
}
