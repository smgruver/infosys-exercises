package problemSolving.basic;

import java.io.*;
import java.util.*;

/*
 * Return the number of words in a given camelCase string
 * Ex: exampleProblem -> 2
 */
public class CamelCase {

    // Complete the camelcase function below.
    static int camelcase(String s) {
    	int result = 1;
        for (char ch : s.toCharArray()) {
            if (new Character(ch).isUpperCase(ch)) {
            	result++;
            }
        }
        
        return result;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String s = scanner.nextLine();

        int result = camelcase(s);

        System.out.println(result);

        scanner.close();
    }
}
