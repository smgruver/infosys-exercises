package structural;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/*
 * Decorator class for Reader to automatically convert alphabetical characters into lowercase.
 */
public class LowercaseReader extends BufferedReader{
	
	private Reader reader;
	
	LowercaseReader(Reader reader){
		super(reader);
		this.reader = reader;
	}

	
	@Override
	public int read() throws IOException{
		int result = reader.read();
//		System.out.println("Read int: " + result + ", as character: " + (char)result);
		if (Character.isAlphabetic(result)) {
			result = Character.toLowerCase(result);
		}
		return result;
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		return reader.read(cbuf, off, len);
	}
	
	@Override
	public String readLine() throws IOException{
		if (reader instanceof BufferedReader) {
			return ((BufferedReader) reader).readLine().toLowerCase();
		} else {
			return new BufferedReader(reader).readLine().toLowerCase();
		}
	}

	@Override
	public void close() throws IOException {
		reader.close();
		
	}
	
	public static void main(String[] args) {
		LowercaseReader decorator;
		try {
			decorator = new LowercaseReader(new BufferedReader(new InputStreamReader(System.in)));
			int input;
			while ((input = decorator.read()) != -1) {
				System.out.print((char)input);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
