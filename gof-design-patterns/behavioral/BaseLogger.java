package behavioral;

public interface BaseLogger {
	
	boolean checkLevel(int level);
	void logMessage(int level, String message);
	BaseLogger getNextLogger();
}
