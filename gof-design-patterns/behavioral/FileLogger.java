package behavioral;

public class FileLogger implements BaseLogger {

	private final int LEVEL = 1;
	private BaseLogger next;
	
	FileLogger(ConsoleLogger nextLevelLogger){
		this.next = nextLevelLogger;
	}
	
	@Override
	public boolean checkLevel(int level) {
		if (level == this.LEVEL) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void logMessage(int level, String message) {
		if (checkLevel(level)) {
			System.out.println("DEBUG: " + message);
		} else {
			if (level > this.LEVEL) {
				getNextLogger().logMessage(level, message);
			}
		}
		return;
	}

	@Override
	public BaseLogger getNextLogger() {
		return next;
	}

}
