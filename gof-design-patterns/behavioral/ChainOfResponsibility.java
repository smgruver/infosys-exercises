package behavioral;

public class ChainOfResponsibility {

	public static void main(String[] args) {
		BaseLogger logger = new FileLogger(new ConsoleLogger(new DBLogger(null)));
		logger.logMessage(1, "debug message");
		logger.logMessage(2, "info message");
		logger.logMessage(3, "warn message");
	}

}
