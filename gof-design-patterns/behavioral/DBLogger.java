package behavioral;

public class DBLogger implements BaseLogger {

	private final int LEVEL = 3;
	private BaseLogger next = null;
	
	DBLogger(BaseLogger nextLevelLogger){
		this.next = nextLevelLogger;
	}
	
	@Override
	public boolean checkLevel(int level) {
		if (level == this.LEVEL) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void logMessage(int level, String message) {
		if (checkLevel(level)) {
			System.out.println("WARN: " + message);
		} else {
			if (level > this.LEVEL) {
				getNextLogger().logMessage(level, message);
			}
		}
		return;
	}

	@Override
	public BaseLogger getNextLogger() {
		return next;
	}

}
