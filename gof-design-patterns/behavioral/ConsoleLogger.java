package behavioral;

public class ConsoleLogger implements BaseLogger {

	private final int LEVEL = 2;
	private BaseLogger next;
	
	ConsoleLogger(DBLogger nextLevelLogger){
		this.next = nextLevelLogger;
	}
	
	@Override
	public boolean checkLevel(int level) {
		if (level == this.LEVEL) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void logMessage(int level, String message) {
		if (checkLevel(level)) {
			System.out.println("INFO: " + message);
		} else {
			if (level > this.LEVEL) {
				getNextLogger().logMessage(level, message);
			}
		}
		return;
	}

	@Override
	public BaseLogger getNextLogger() {
		return next;
	}

}
