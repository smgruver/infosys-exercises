package creational;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Singleton implements Serializable{

	private static Singleton instance;
	
	private Singleton() {
		
	}
	
	public static Singleton getSingleton() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	public static void main(String[] args) {
		System.out.println("Written singleton: " + instance.getSingleton());
		try (ObjectOutputStream obOut = new ObjectOutputStream(new FileOutputStream("singletonobject.ser"));){
			 obOut.writeObject(instance.getSingleton());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try (ObjectInputStream obIn = new ObjectInputStream(new FileInputStream("singletonobject.ser"));){
			 Singleton newSingle = (Singleton)(obIn.readObject());
			 System.out.println("Read singleton: " + newSingle);
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}

}
